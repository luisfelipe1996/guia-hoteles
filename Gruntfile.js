module.exports = function (grunt){
	grunt.initConfig({
		sass: {
			dist: {
				files: [{
					expand: true,
					cwd: 'css',
					src: ['*.scss'],
					dest: 'css',
					ext: '.css'
				}]
			}
		},
		watch:{
			files: ['css/*.scss'],
			tasks: ['css']
		},
		browserSync:{
			dev:{
				bsFiles:{
					src: [
					'css/*.css',
					'*.html',
					'js/*.js'
					]
				},
				options: {
					watchTask: true,
					server: {
						baseDir: './'
					}
				}
			}
		},
		imagemin: {
			dynamic: {
				files :[{
					expand: true,
					cwd: './',
					src: 'img/*.{png,gif,jpg,jpeg}',
					dest: 'dist/'
				}]
			}
		},
	});

	grunt.loadNpmTask('grunt-contib-watch');
	grunt.loadNpmTask('grunt-contib-watch');
	grunt.loadNpmTask('grunt-browser-sync');
	grunt.loadNpmTask('grunt-contrib-imagemin');
	grunt.registerTask('css',['sass']);
	grunt.registerTask('default',['browserSync', 'watch']);
	grunt.registerTask('img:compress',['imagemin']);
};